/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.animal;

/**
 *
 * @author MSI
 */
public class Fish extends AquaticAnimal{
    private String nickname;
    public Fish(String nickname) {
        super(nickname,0);
        this.nickname = nickname;
    }

    @Override
    public void swim() {
        System.out.println("Fish" + nickname + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Fish" + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Fish" + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Fish" + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Fish" + nickname + " sleep");
    }
    @Override
    public String toString(){
        return "Animal nickname: " + nickname + super.toString();
    } 
    
}
