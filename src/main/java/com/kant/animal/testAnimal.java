/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.animal;

/**
 *
 * @author MSI
 */
public class testAnimal {
    public static void main(String[] args) {
        Human h1 = new Human ("Kant");
        System.out.println(h1);
        h1.run();  
        h1.eat();
        h1.walk();   
        h1.speak();
        h1.sleep();
             
        System.out.println();
        
        Cat c1 = new Cat("Michael");
        System.out.println(c1);
        c1.run(); 
        c1.eat();
        c1.walk();         
        c1.speak();
        c1.sleep(); 
        
        System.out.println();
        
        Dog d1 = new Dog("Dang");
        System.out.println(d1);
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        
        System.out.println();
        
        Crocodile cr1 = new Crocodile("Fedfe");
        System.out.println(cr1);
        cr1.crawl();
        cr1.eat();
        cr1.walk(); 
        cr1.speak();
        cr1.sleep();
        
        System.out.println();
        
        Snake s1 = new Snake("Leuam");
        System.out.println(s1);
        s1.crawl();
        s1.eat();
        s1.walk();;
        s1.speak();
        s1.sleep();
        
        System.out.println();
        
        Fish f1 = new Fish("Nemo");
        System.out.println(f1);
        f1.swim();
        f1.eat();
        f1.walk();      
        f1.speak();
        f1.sleep();
        
        System.out.println();
        
        Crab pu1 = new Crab("Zoze");
        System.out.println(pu1);
        pu1.swim();
        pu1.eat();
        pu1.walk();       
        pu1.speak();
        pu1.sleep();
        
        System.out.println();
        
        Bat kk1 = new Bat("Zero");
        System.out.println(kk1);
        kk1.fly();
        kk1.eat();
        kk1.walk();
        kk1.speak();
        kk1.sleep();
        
        System.out.println();
        
        Bird b1 = new Bird("Sky");
        System.out.println(b1);
        b1.fly();
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        
        System.out.println();
        
        Animal[ ] animals = {h1 , c1 , d1 , cr1 , s1 , f1 , pu1 , kk1 , b1};
        for (int i = 0; i < animals.length; i++){
            System.out.println(animals[i].getName() + " Instanceof Animal " + (animals[i] instanceof Animal));
            System.out.println(animals[i].getName() + " Instanceof LandAnimal " + (animals[i] instanceof LandAnimal));
            System.out.println(animals[i].getName() + " Instanceof Reptile " + (animals[i] instanceof Reptile));        
            System.out.println(animals[i].getName() + " Instanceof AquaticAnima " + (animals[i] instanceof AquaticAnimal));
            System.out.println(animals[i].getName() + " Instanceof Poultry " + (animals[i] instanceof Poultry));
        }
            
        
                
         
        
       
        
                
                
                
                
                
                
                
                
                
                
                
                
                
        //System.out.println("h1 is animal? " + (h1 instanceof Animal));
        //System.out.println("h1 is land animal? " + (h1 instanceof LandAnimal));
        
       //Animal a1 = h1;
       //System.out.println("a1 is land animal? " + (a1 instanceof LandAnimal));
       //System.out.println("a1 is reptile animal? " + (a1 instanceof Reptile));
        
        
        
        
        
        
        
        
        
        
    }
}
